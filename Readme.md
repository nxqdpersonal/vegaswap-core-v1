# Vegaswap V1

[![Tests](https://github.com/vegaswap/vegaswap-core-v1/actions/workflows/tests.yml/badge.svg)](https://github.com/vegaswap/vegaswap-core-v1/actions/workflows/tests.yml)


This repository contains the core smart contracts for the Vegaswap V2 Protocol.

# Guide

```sh
make compile # compile
make test # run test
```